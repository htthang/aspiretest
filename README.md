# selenium-testng-template

A template project to seep up automation test. 

It's based on:
1. Java 8
2. Maven
3. TestNG
4. Selenium 3
5. logback


Functions:
1. Login on EBANQ page
2. Create a transfer between accounts

How to use:
1. Open terminal on Mac 
2. Type "cd {directory this project}"
3. Type:  mvn test -Dfile="src/test/resources/test/main/cases/test/TransferBetweenAccountsSuite.xml"

