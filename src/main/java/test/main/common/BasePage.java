package test.main.common;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The base page
 *
 */
public abstract class BasePage {
	protected static RemoteWebDriver driver = Config.driver();
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
}
