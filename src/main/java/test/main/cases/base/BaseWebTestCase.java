package test.main.cases.base;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.*;

import test.main.common.Config;
import test.main.pages.*;
import test.main.pages.ivy.LoginIvyPageObject;
import test.main.pages.ivy.SignUpPageObject;


public abstract class BaseWebTestCase extends BaseTestCase {
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	public LoginEBANQPageObject loginObject=new LoginEBANQPageObject();
	public LoginIvyPageObject loginIvyObject=new LoginIvyPageObject();
	public SignUpPageObject signUpObject=new SignUpPageObject();
	public Navigation navigation=new Navigation();
	public TransferBetweenAccountsPageObject transferBetweenAccounts=new TransferBetweenAccountsPageObject();
	@BeforeTest
	@Parameters({ "browser", "url" })
	public void setupDriver(@Optional String browser, @Optional String url) throws Exception {
		Config.setupWebDriver(browser, url);
		log.info("Setup web driver: " + browser);
	}

	@AfterTest
	public void tearDown() {
		super.tearDown();
		if (Config.driver() != null) {
			Config.driver().quit();
		}
	}

	protected RemoteWebDriver driver() {
		return Config.driver();
	}
	public void browseURL(String url) {
		driver().get(url);
	}
}
