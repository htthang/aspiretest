package test.main.cases.test;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import test.main.cases.base.BaseWebTestCase;
import test.main.utils.SeleniumUtils;

public class LoginIVY extends BaseWebTestCase {
    @BeforeClass
    public void LaunchBrowser(){
        driver().get("https://link-staging.saas.ivy.com/auth/login");
        loginIvyObject.loginWithPassWord("hani+160801@offspringdigital.com", "123456qQ@");
        SeleniumUtils.takeScreenShot();
    }

    @Test()
    public void SignInIvy() {
        loginIvyObject.selectTenant();
    }



}
