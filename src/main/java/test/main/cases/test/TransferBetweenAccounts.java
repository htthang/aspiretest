package test.main.cases.test;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import test.main.cases.base.BaseWebTestCase;
import test.main.utils.SeleniumUtils;

public class TransferBetweenAccounts extends BaseWebTestCase {
    @BeforeClass
    public void LaunchBrowser(){

        driver().get("https://demo.ebanq.com/log-in");
        loginObject.loginEBANQ("Bank-Admin", "Demo-Access1");
        SeleniumUtils.takeScreenShot();
    }

    @Test()
    public void CreateTransferBetweenAccount() {
        navigation.gotoTransferPage();
        navigation.gotoTransferBetweenAccounts();
        transferBetweenAccounts.selectUser("Mary Johnson (mjohnson)");
        transferBetweenAccounts.selectDebitAccount("EBQ12123423456");
        transferBetweenAccounts.selectCreditAccount("EBQ11123412345");
        transferBetweenAccounts.inputTransferAmount("1");
        transferBetweenAccounts.inputDescription("Admin user triggered auto transfer with amount ");
        transferBetweenAccounts.clickBtnSubmit();
        transferBetweenAccounts.clickBtnConfirm();
        transferBetweenAccounts.verifyResultCreateAccount();
    }
}

