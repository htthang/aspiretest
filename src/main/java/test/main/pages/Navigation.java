package test.main.pages;

import org.openqa.selenium.By;
import test.main.common.BasePage;
import test.main.utils.SeleniumUtils;
import test.main.utils.SeleniumWaitUtils;

public class Navigation extends BasePage {
    private By lnkTransfer = By.xpath("//div[@routerlink='/transfer']");
    private By lnkTransferBetweenAccounts = By.xpath("//div[@ng-reflect-router-link='admin/transfer-between-account']");

    public void gotoTransferPage() {
        log.info("Go to Transfer page");
        SeleniumWaitUtils.waitForElement(lnkTransfer);
        SeleniumUtils.click(lnkTransfer);
    }

    public void gotoTransferBetweenAccounts() {
        log.info("Go to Transfer Between Accounts page");
        SeleniumWaitUtils.waitForElement(lnkTransferBetweenAccounts);
        SeleniumUtils.click(lnkTransferBetweenAccounts);
    }
}
