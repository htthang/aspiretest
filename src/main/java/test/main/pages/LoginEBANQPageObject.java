package test.main.pages;

import org.openqa.selenium.By;
import org.testng.Assert;
import test.main.common.BasePage;
import test.main.common.Config;
import test.main.utils.SeleniumUtils;
import test.main.utils.SeleniumWaitUtils;

public class LoginEBANQPageObject extends BasePage {
    private By txtUserName = By.xpath("//a[contains(text(),'Have a password?')]");
    private By txtPassword = By.xpath("//input[@formcontrolname='password']");
    private By btnSignIn = By.xpath("//button[contains(text(),'Sign In')]");
    private By verifyResult = By.xpath("//span[contains(text(),'Hello, John Banker')]");

    public void loginEBANQ(String UserName, String Password) {
        inputUserName(UserName);
        inputPassword(Password);
        clickOnSignIn();
        VerifyResultLogin();
    }

    public void inputUserName(String userName) {
        log.info("Input user name: " + userName);
        SeleniumUtils.sendKeys(txtUserName, userName);
    }

    public void inputPassword(String password) {
        log.info("Input password: " + password);
        SeleniumUtils.sendKeys(txtPassword, password);
    }

    public void clickOnSignIn() {
        log.info("Click on login button");
        SeleniumUtils.click(btnSignIn);
    }

    public void VerifyResultLogin(){
        log.info("Verify login successfully");
        SeleniumWaitUtils.waitForElement(verifyResult);
        String actual = Config.driver().findElement(verifyResult).getText();
        Assert.assertEquals(actual, "Hello, John Banker");
    }
}
