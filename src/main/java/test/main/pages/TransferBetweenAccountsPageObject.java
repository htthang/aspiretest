package test.main.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import test.main.common.BasePage;
import test.main.common.Config;
import test.main.utils.SeleniumUtils;
import test.main.utils.SeleniumWaitUtils;

import java.util.List;

public class TransferBetweenAccountsPageObject extends BasePage {
    private By clickUser = By.xpath("//div[contains(@class,'input-field select-container')]");
    private By getAllItems = By.xpath("//div[contains(@class,'ng-dropdown')]/div[2]/div/span");
    private By clickDebitAccount = By.xpath("(//*[contains(text(),'Select account')])[1]/../../../..");
    private By getAllAccounts = By.xpath("//div[contains(@class,'ng-dropdown')]/div[2]/div/span");
    private By clickCreditAccount = By.xpath("(//*[contains(text(),'Select account')])[2]/../../../..");
    private By txtAmountTransfer = By.xpath("//input[contains(@class,'native-input amount')]");
    private By txtDescription = By.xpath("//textarea[(@id='description')]");
    private By btnSubmit = By.xpath("//button[(@type='submit')]");
    private By verifyResult = By.xpath("//div[(@class='popup-message')]");


    public void selectUser(String user){
        log.info("Select an user");
        selectFromDropdown(clickUser,getAllItems,user);
    }

    public void selectDebitAccount(String account){
        log.info("Select debit from list account");
        selectFromDropdown(clickDebitAccount,getAllAccounts,account);
    }

    public void selectCreditAccount(String account){
        log.info("Select credit from list account");
        selectFromDropdown(clickCreditAccount,getAllAccounts,account);
    }

    public void inputTransferAmount(String amount){
        log.info("Input transfer for amount");
        SeleniumUtils.sendKeys(txtAmountTransfer,amount);
    }

    public void inputDescription(String description){
        WebElement accountField = Config.driver().findElement(txtAmountTransfer);
        String getAmount = accountField.getAttribute("value");
        log.info("Input content at description is" + getAmount);
        SeleniumWaitUtils.waitForElement(txtDescription);
        SeleniumUtils.sendKeys(txtDescription,description  +  getAmount);
    }

    public void clickBtnSubmit(){
        log.info("Click on Submit button");
        SeleniumWaitUtils.waitForElement(btnSubmit);
        SeleniumUtils.click(btnSubmit);
    }

    public void clickBtnConfirm(){
        log.info("Click on Confirm button");
        SeleniumWaitUtils.waitForElement(btnSubmit);
        SeleniumUtils.click(btnSubmit);
    }

    public void verifyResultCreateAccount(){
        log.info("Verify create account successfully");
        SeleniumWaitUtils.waitForElement(verifyResult);
        String actual = Config.driver().findElement(verifyResult).getText();
        Assert.assertEquals(actual, "Transfer has been executed successfully.\n" +
                "Back to transfers.");
    }

    public void selectFromDropdown(By xpathParent, By getItem, String expectedValue)  {
        SeleniumWaitUtils.waitForElement(xpathParent);
        Config.driver().findElement(xpathParent).click();
        SeleniumWaitUtils.waitForElement(getItem);
            List<WebElement> myElements = Config.driver().findElements(getItem);
            System.out.println("The total number of elements is " + myElements.size());
            for(WebElement element : myElements) {
                System.out.println(element.getText());
                if (element.getText().trim().equals(expectedValue)) {
                    if (element.isDisplayed()) {
                        System.out.println("Select value is " + element.getText());
                        element.click();
                    } else {
                        JavascriptExecutor js = (JavascriptExecutor) Config.driver();
                        js.executeScript("arguments[0].scrollIntoView(true);",element);
                        System.out.println("Select value is" + element.getText());
                        js.executeScript("arguments[0].click();",element);
                    }
                    break;
                }
            }
        }
}
