package test.main.pages.ivy;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import test.main.common.BasePage;
import test.main.common.Config;
import test.main.utils.SeleniumUtils;
import test.main.utils.SeleniumWaitUtils;
import java.util.List;
import java.util.Random;

public class LoginIvyPageObject extends BasePage {
    private By havePassWord = By.xpath("//a[contains(text(),'Have a password?')]");
    private By inputEmail= By.xpath("//input[@type='email']");
    private By inputPassword= By.xpath("//input[@type='password']");
    private By btnLogin= By.xpath("//button[@type='submit']");
    private By verifyTextAccount= By.xpath("//h2[contains(text(),'Select Account')]");
    private By VerifyListTenant= By.xpath("//button[contains(@class, 'c1') and contains(@class, 'tenant')]");


    public void loginWithPassWord(String Email, String Password) {
        clickHavePassword();
        inputEmail(Email);
        inputPassword(Password);
        clickOnSubmit();
        SeleniumWaitUtils.waitForElement(verifyTextAccount);
        verifyTextTenant("Select Account");
    }
    private void clickHavePassword() {
        SeleniumUtils.click(havePassWord);
    }

    private void inputEmail(String Email) {
        log.info("Input email: " + Email);
        SeleniumUtils.sendKeys(inputEmail,Email);
    }
    private void inputPassword(String password) {
        log.info("Input password: " + password);
        SeleniumUtils.sendKeys(inputPassword, password);
    }
    public void clickOnSubmit() {
        log.info("Click on login button");
        SeleniumUtils.click(btnLogin);
    }

    public void verifyTextTenant(String text) {
        log.info("Verify t select account: " + text);
        String actualMessage = Config.driver().findElement(verifyTextAccount).getText();
        Assert.assertEquals(actualMessage, text);
    }

    public void getRandomTenantButton(By elementTenant, By elementWait) {
        // Find all products
        List<WebElement> allTenants = Config.driver().findElements(elementTenant);
        // Check if the list is not empty
        if (allTenants.isEmpty()) {
            throw new RuntimeException("No products found!");
        }
        // Select a random product
        Random rand = new Random();
        int randomProductIndex = rand.nextInt(allTenants.size());
        allTenants.get(randomProductIndex).click();
        // Wait for the next element to be present
        SeleniumWaitUtils.waitForElement(elementWait);
    }

    public void selectTenant(){
        getRandomTenantButton(VerifyListTenant,VerifyListTenant);
    }

    // need to choose a tenant before login
    public void loginWithPassWordAndTenant(String Email, String Password) {
        selectTenant();
        loginWithPassWord(Email, Password);
    }

}
