package test.main.pages.ivy;

import org.openqa.selenium.By;
import test.main.utils.SeleniumUtils;
import test.main.utils.SeleniumWaitUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class SignUpPageObject {
    private By clickSignUp= By.xpath("//button[contains(text(),'SIGN UP')]");
    private By clickActiveAccount= By.xpath("//a[contains(text(),'ACTIVE YOUR ACCOUNT')]");
    private By inputEmail= By.xpath("//input[@type='email']");
    private By btnSignUp= By.xpath("//button[contains(text(),'CONTINUE')]");
    private By clickGoogle= By.xpath("//a[@class='login-google ng-star-inserted']");
    private By clickAnotherAccount= By.xpath("//div[contains(text(),'Use another account')]");
    private By inputEmailGoogle= By.xpath("//input[@id='identifierId']");
    private By btnNext= By.xpath("//span[contains(text(),'Next')]");
    private By inputPasswordGoogle= By.xpath("//input[@name='Passwd']");
    private By btnContinue= By.xpath("//button[span[text()='Continue']]");




    //Click on Sign Up button
    public void clickSignUp() {
        SeleniumUtils.click(clickSignUp);
    }

    //Click on Active Account button
    public void clickActiveAccount() {
        SeleniumUtils.click(clickActiveAccount);
    }

    //Input email
    public void inputEmail(String Email) {
        SeleniumUtils.sendKeys(inputEmail,Email);
    }

    //Click on Sign Up button
    public void clickOnSignUp() {
        SeleniumUtils.click(btnSignUp);
    }

    //Click on Google button
    public void clickGoogle() {
        SeleniumUtils.click(clickGoogle);
    }

    //Click on Another Account button
    public void clickAnotherAccount() {
        SeleniumUtils.click(clickAnotherAccount);
    }

    //Input email for Google
    public void inputEmailGoogle(String Email) {
        SeleniumUtils.sendKeys(inputEmailGoogle,Email);
    }

    //Click on Next button
    public void clickNext() {
        SeleniumUtils.click(btnNext);
    }

    //Input password for Google
    public void inputPasswordGoogle(String Password) {
        SeleniumUtils.sendKeys(inputPasswordGoogle,Password);
    }

    //Click on Continue button
    public void clickContinue() {
        SeleniumUtils.click(btnContinue);
    }


    //Sign up with Google
    public void signUpWithGoogle() {
        clickSignUp();
        clickActiveAccount();
        SeleniumWaitUtils.waitForElement(clickGoogle);
        clickGoogle();
        clickAnotherAccount();
        inputEmailGoogle("autotest1608");
        clickNext();
        inputPasswordGoogle("123456aA@");
        clickContinue();

    }



    //Sign up with email
    public void signUpWithEmail() {
        String email = "hani+" + SeleniumUtils.getRandomNumber() + "@offspringdigital.com";
        clickSignUp();
        clickActiveAccount();
        inputEmail(email);
        clickOnSignUp();
    }


}
